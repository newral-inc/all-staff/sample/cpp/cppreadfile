﻿// CPPReadFile.cpp : このファイルには 'main' 関数が含まれています。プログラム実行の開始と終了がそこで行われます。
//

#include "pch.h"
#include <iostream>
#include <fstream>>
#include <string>

int main()
{
	// ファイル名を指定します。
	// 絶対パスか相対パスで指定します。
	// ファイル名のみの場合、カレントディレクトリのファイルが対象となります。
	std::string fileName = "test.txt";

	// ファイルを開きます。
	std::ifstream fin;
	fin.open(fileName, std::ios::in);
	
	// ファイルが開けない場合
	if (!fin)
	{
		// エラーメッセージを表示して終了します。
		std::cout << "ファイルが開けません" << std::endl;
		return -1;
	}

	// ファイルの終端まで繰り返します。
	std::string line;
	while (!fin.eof())
	{
		// 1行データを取得取得します。
		std::getline(fin, line);

		// 取得した1行データをコンソールに表示します。
		std::cout << line << std::endl;
	}

	// ファイルを閉じます。
	fin.close();
	return 0;
}
